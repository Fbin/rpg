﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Engine;
using System.IO;

namespace RPG
{
    public partial class SuperAdventure : Form
    {
        private Player player;
        private Enemies enemie;
        private const string PLAYER_DATA_FILE_NAME = "PlayerData.xml";

        public SuperAdventure()
        {
            InitializeComponent();

            if (File.Exists(PLAYER_DATA_FILE_NAME))
            {
                player = Player.CreatePlayerFromXmlString(File.ReadAllText(PLAYER_DATA_FILE_NAME));
            }
            else
            {
                player = Player.createDefaultPlayer();
            }

            lblHP.DataBindings.Add("Text", player, "hp");
            lblGold.DataBindings.Add("Text", player, "gold");
            lblExp.DataBindings.Add("Text", player, "exp");
            lblLvl.DataBindings.Add("Text", player, "lvl");

            dgvInventory.RowHeadersVisible = false;
            dgvInventory.AutoGenerateColumns = false;

            dgvInventory.DataSource = player.inventory;

            dgvInventory.Columns.Add(new DataGridViewTextBoxColumn
            {
                HeaderText = "Name",
                Width = 197,
                DataPropertyName = "Description"
            });

            dgvInventory.Columns.Add(new DataGridViewTextBoxColumn
            {
                HeaderText = "Quantity",
                DataPropertyName = "Quantity"
            });

            dgvQuests.RowHeadersVisible = false;
            dgvQuests.AutoGenerateColumns = false;

            dgvQuests.DataSource = player.playerQuest;

            dgvQuests.Columns.Add(new DataGridViewTextBoxColumn
            {
                HeaderText = "Name",
                Width = 197,
                DataPropertyName = "Name"
            });

            dgvQuests.Columns.Add(new DataGridViewTextBoxColumn
            {
                HeaderText = "Done?",
                DataPropertyName = "IsCompleted"
            });

            cboWeapons.DataSource = player.weapons;
            cboWeapons.DisplayMember = "Name";
            cboWeapons.ValueMember = "Id";

            if (player.currentWeapon != null)
            {
                cboWeapons.SelectedItem = player.currentWeapon;
            }

            cboWeapons.SelectedIndexChanged += cboWeapons_SelectedIndexChanged;

            cboPotions.DataSource = player.potions;
            cboPotions.DisplayMember = "Name";
            cboPotions.ValueMember = "Id";

            player.PropertyChanged += PlayerOnPropertyChanged;

            MoveTo(player.currentLocation);
        }

        private void DisplayMessage(object sender, MessageEventArgs messageEventArgs)
        {
            rtbMessages.Text += messageEventArgs.Message + Environment.NewLine;

            if (messageEventArgs.AddExtraNewLine)
            {
                rtbMessages.Text += Environment.NewLine;
            }

            rtbMessages.SelectionStart = rtbMessages.Text.Length;
            rtbMessages.ScrollToCaret();
        }

        private void btnWest_Click(object sender, EventArgs e)
        {
            player.MoveWest();
        }

        private void btnNorth_Click(object sender, EventArgs e)
        {
            player.MoveNorth();
        }

        private void btnSouth_Click(object sender, EventArgs e)
        {
            player.MoveSouth();
        }

        private void btnEast_Click(object sender, EventArgs e)
        {
            player.MoveEast();
        }

        private void MoveTo(Location newLocation)
        {
            //Does the location have any required items
            if (!player.hasRequredItemToEnterThisLocation(newLocation))
            {
                rtbMessages.Text += "You must have a " + newLocation.requiredToEnter.name + " to enter this location." + Environment.NewLine;
                scrollToEnd();
                return;
            }

            /*else if (!player.hasRequiredLvlToEnterThisLocation(newLocation))
            {
                rtbMessages.Text += "Your level isn't suited to travel past this point. If you wish to experience further adventures, you need to reach " + newLocation.requiredLvlToEnter + " ." + Environment.NewLine;
                scrollToEnd();
                return;
            }*/

            // Update the player's current location
            player.currentLocation = newLocation;

            // Show/hide available movement buttons
            btnNorth.Visible = (newLocation.locationToNorth != null);
            btnEast.Visible = (newLocation.locationToEast != null);
            btnSouth.Visible = (newLocation.locationToSouth != null);
            btnWest.Visible = (newLocation.locationToWest != null);

            // Display current location name and description
            rtbLocation.Text = newLocation.name + Environment.NewLine;
            rtbLocation.Text += newLocation.description + Environment.NewLine;

            // Completely heal the player
            player.hp = player.maxHp;

            // Does the location have a quest?
            if (newLocation.questAvailableHere != null)
            {
                // See if the player already has the quest, and if they've completed it
                bool playerAlreadyHasQuest = player.HasThisQuest(newLocation.questAvailableHere);
                bool playerAlreadyCompletedQuest = player.CompletedThisQuest(newLocation.questAvailableHere);

                // See if the player already has the quest
                if (playerAlreadyHasQuest)
                {
                    // If the player has not completed the quest yet
                    if (!playerAlreadyCompletedQuest)
                    {
                        // See if the player has all the items needed to complete the quest
                        bool playerHasAllItemsToCompleteQuest = player.HasAllQuestCompletionItems(newLocation.questAvailableHere);

                        // The player has all items required to complete the quest
                        if (playerHasAllItemsToCompleteQuest)
                        {
                            //Display message
                            rtbMessages.Text += Environment.NewLine;
                            rtbMessages.Text += "You complete the '" + newLocation.questAvailableHere.name + "' quest." + Environment.NewLine;

                            //Remove quest items from inventory
                            player.RemoveQuestCompletionItems(newLocation.questAvailableHere);

                            // Give quest rewards
                            rtbMessages.Text += "You receive: " + Environment.NewLine;
                            rtbMessages.Text += newLocation.questAvailableHere.expGain.ToString() + " experience points" + Environment.NewLine;
                            rtbMessages.Text += newLocation.questAvailableHere.reward.ToString() + " gold" + Environment.NewLine;
                            rtbMessages.Text += newLocation.questAvailableHere.rewardItem.name + Environment.NewLine;
                            rtbMessages.Text += Environment.NewLine;
                            scrollToEnd();

                            player.exp += newLocation.questAvailableHere.expGain;
                            player.gold += newLocation.questAvailableHere.reward;
                            if (player.exp >= 100)
                            {
                                player.lvlUp();
                            }
                            // Add the reward item to the player's inventory
                            player.AddItemToInventory(newLocation.questAvailableHere.rewardItem);

                            // Mark the quest as completed
                            player.MarkQuestCompleted(newLocation.questAvailableHere);
                        }
                    }
                }
                else
                {
                    // The player does not already have the quest

                    // Display the messages
                    rtbMessages.Text += "You receive the " + newLocation.questAvailableHere.name + " quest." + Environment.NewLine;
                    rtbMessages.Text += newLocation.questAvailableHere.description + Environment.NewLine;
                    rtbMessages.Text += "To complete it, return with:" + Environment.NewLine;
                    foreach (QuestReward qci in newLocation.questAvailableHere.questReward)
                    {
                        if (qci.quantity == 1)
                        {
                            rtbMessages.Text += qci.quantity.ToString() + " " + qci.details.name + Environment.NewLine;
                        }
                        else
                        {
                            rtbMessages.Text += qci.quantity.ToString() + " " + qci.details.nameplural + Environment.NewLine;
                        }
                    }
                    rtbMessages.Text += Environment.NewLine;
                    scrollToEnd();

                    // Add the quest to the player's quest list
                    player.playerQuest.Add(new PlayerQuest(newLocation.questAvailableHere));
                }
            }

            // Does the location have a monster?
            if (newLocation.enemiesAvailableHere != null)
            {
                rtbMessages.Text += "You see a " + newLocation.enemiesAvailableHere.name + Environment.NewLine;
                scrollToEnd();

                // Make a new monster, using the values from the standard monster in the World.Monster list
                Enemies standardMonster = World.MonsterByID(newLocation.enemiesAvailableHere.id);

                enemie = new Enemies(standardMonster.id, standardMonster.name, standardMonster.hp, standardMonster.maxHp, standardMonster.dmg,
                    standardMonster.expGain, standardMonster.reward);

                foreach (Loot lootItem in standardMonster.loot)
                {
                    enemie.loot.Add(lootItem);
                }

                cboWeapons.Visible = true;
                cboPotions.Visible = true;
                btnUseWeapon.Visible = true;
                btnUsePotion.Visible = true;
            }
            else
            {
                enemie = null;

                cboWeapons.Visible = false;
                cboPotions.Visible = false;
                btnUseWeapon.Visible = false;
                btnUsePotion.Visible = false;
            }
        }        

        private void btnUseWeapon_Click(object sender, EventArgs e)
        {
            // Get the currently selected weapon from the cboWeapons ComboBox
            Weapons currentWeapon = (Weapons)cboWeapons.SelectedItem;

            player.UseWeapon(currentWeapon);
        }

        private void btnUsePotion_Click(object sender, EventArgs e)
        {
            // Get the currently selected potion from the combobox
            Potion potion = (Potion)cboPotions.SelectedItem;

            player.UsePotion(potion);
        }

        private void cboWeapons_SelectedIndexChanged(object sender, EventArgs e)
        {
            player.currentWeapon = (Weapons)cboWeapons.SelectedItem;
        }

        private void PlayerOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (propertyChangedEventArgs.PropertyName == "Weapons")
            {
                cboWeapons.DataSource = player.weapons;

                if (!player.weapons.Any())
                {
                    cboWeapons.Visible = false;
                    btnUseWeapon.Visible = false;
                }
            }

            if (propertyChangedEventArgs.PropertyName == "Potions")
            {
                cboPotions.DataSource = player.potions;

                if (!player.potions.Any())
                {
                    cboPotions.Visible = false;
                    btnUsePotion.Visible = false;
                }
            }

            if (propertyChangedEventArgs.PropertyName == "CurrentLocation")
            {
                // Show/hide available movement buttons
                btnNorth.Visible = (player.currentLocation.locationToNorth != null);
                btnEast.Visible = (player.currentLocation.locationToEast != null);
                btnSouth.Visible = (player.currentLocation.locationToSouth != null);
                btnWest.Visible = (player.currentLocation.locationToWest != null);
                btnTrade.Visible = (player.currentLocation.vendorAvailableHere != null);

                // Display current location name and description
                rtbLocation.Text = player.currentLocation.name + Environment.NewLine;
                rtbLocation.Text += player.currentLocation.description + Environment.NewLine;

                if (player.currentLocation.enemiesAvailableHere == null)
                {
                    cboWeapons.Visible = false;
                    cboPotions.Visible = false;
                    btnUseWeapon.Visible = false;
                    btnUsePotion.Visible = false;
                }
                else
                {
                    cboWeapons.Visible = player.weapons.Any();
                    cboPotions.Visible = player.potions.Any();
                    btnUseWeapon.Visible = player.weapons.Any();
                    btnUsePotion.Visible = player.potions.Any();
                }
            }
        }

        private void btnTrade_Click(object sender, EventArgs e)
        {
            TradingScreen tradingScreen = new TradingScreen(player);
            tradingScreen.StartPosition = FormStartPosition.CenterParent;
            tradingScreen.ShowDialog(this);
        }

        //saves the game when closing window
        /*private void SuperAdventure_FormClosing(object sender, FormClosingEventArgs e)
        {
            File.WriteAllText(PLAYER_DATA_FILE_NAME, player.ToXmlString());
        }*/

        private void scrollToEnd()
        {
            rtbMessages.SelectionStart = rtbMessages.Text.Length;
            rtbMessages.ScrollToCaret();
        }

        private void btnMenu_Click(object sender, EventArgs e)
        {
            MenuScreen menuScreen = new MenuScreen(player);
            menuScreen.StartPosition = FormStartPosition.CenterParent;
            menuScreen.ShowDialog(this);
        }
    }
}

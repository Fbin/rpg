﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine
{
    public class Enemies : LivingCreatures
    {
        public int id;
        public string name;
        public string nameplural;
        public int dmg;
        public int expGain;
        public int reward;
        public List<Loot> loot;

        public Enemies(int id, string name, int hp, int maxHp, int dmg, int expGain, int reward) : base(hp, maxHp)
        {
            this.id = id;
            this.name = name;
            this.dmg = dmg;
            this.expGain = expGain;
            this.reward = reward;
            loot = new List<Loot>();
        }
    }
}

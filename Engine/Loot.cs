﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine
{
    public class Loot
    {
        public Items details;
        public int dropRate;
        public bool defaultItem;

        public Loot(Items details, int dropRate, bool defaultItem)
        {
            this.details = details;
            this.dropRate = dropRate;
            this.defaultItem = defaultItem;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine
{
    public class Quest
    {
        public int id;
        public string name;
        public string description;
        public int expGain;
        public int reward;
        public Items rewardItem;
        public List<QuestReward> questReward;

        public Quest(int id, string name, string description, int expGain, int reward)
        {
            this.id = id;
            this.name = name;
            this.description = description;
            this.expGain = expGain;
            this.reward = reward;
            questReward = new List<QuestReward>();
        }
    }
}

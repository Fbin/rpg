﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine
{
    public class Inventory : INotifyPropertyChanged
    {
        private Items _details;
        private int _quantity;

        public Items details
        {
            get { return _details; }
            set
            {
                _details = value;
                OnPropertyChanged("details");
            }
        }
        public int quantity
        {
            get { return _quantity; }
            set
            {
                _quantity = value;
                OnPropertyChanged("quantity");
                OnPropertyChanged("description");
            }
        }
        public string description
        {
            get { return quantity > 1 ? details.nameplural : details.name; }
        }
        public int itemId
        {
            get { return details.id; }
        }
        public float price
        {
            get { return details.price; }
        }

        public Inventory(Items details, int quantity)
        {
            this.details = details;
            this.quantity = quantity;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}

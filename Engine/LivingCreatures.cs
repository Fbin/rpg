﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine
{
    public class LivingCreatures : INotifyPropertyChanged
    {
        private int _hp;

        public int hp
        {
            get { return _hp; }
            set
            {
                _hp = value;
                OnPropertyChanged("hp");
            }
        }
        public int maxHp;

        public LivingCreatures(int hp, int maxHp)
        {
            this.hp = hp;
            this.maxHp = maxHp;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged!= null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}

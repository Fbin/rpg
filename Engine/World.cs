﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine
{
    public static class World
    {
        //Lists for all objects
        public static readonly List<Items> Items = new List<Items>();
        public static readonly List<Enemies> Monsters = new List<Enemies>();
        public static readonly List<Quest> Quests = new List<Quest>();
        public static readonly List<Location> Locations = new List<Location>();

        //constants for id access without remembering the exact number
        public const int ITEM_ID_RUSTY_SWORD = 1;
        public const int ITEM_ID_RAT_TAIL = 2;
        public const int ITEM_ID_PIECE_OF_FUR = 3;
        public const int ITEM_ID_SNAKE_FANG = 4;
        public const int ITEM_ID_SNAKESKIN = 5;
        public const int ITEM_ID_CLUB = 6;
        public const int ITEM_ID_HEALING_POTION = 7;
        public const int ITEM_ID_SPIDER_FANG = 8;
        public const int ITEM_ID_SPIDER_SILK = 9;
        public const int ITEM_ID_ADVENTURER_PASS = 10;
        public const int ITEM_ID_SPIKED_CLUB = 11;
        public const int ITEM_ID_GOLDEN_COLT = 12;
        public const int ITEM_ID_LEATHER = 13;
        public const int ITEM_ID_ROTTEN_FLESH = 14;
        public const int ITEM_ID_BONE = 15;
        public const int ITEM_ID_LANCE = 16;

        public const int MONSTER_ID_RAT = 1;
        public const int MONSTER_ID_SNAKE = 2;
        public const int MONSTER_ID_GIANT_SPIDER = 3;
        public const int MONSTER_ID_TROLL = 4;
        public const int MONSTER_ID_ZOMBIE = 5;
        public const int MONSTER_ID_SKELETON = 6;

        public const int QUEST_ID_CLEAR_ALCHEMIST_GARDEN = 1;
        public const int QUEST_ID_CLEAR_FARMERS_FIELD = 2;

        public const int LOCATION_ID_HOME = 1;
        public const int LOCATION_ID_TOWN_SQUARE = 2;
        public const int LOCATION_ID_GUARD_POST = 3;
        public const int LOCATION_ID_ALCHEMIST_HUT = 4;
        public const int LOCATION_ID_ALCHEMISTS_GARDEN = 5;
        public const int LOCATION_ID_FARMHOUSE = 6;
        public const int LOCATION_ID_FARM_FIELD = 7;
        public const int LOCATION_ID_BRIDGE = 8;
        public const int LOCATION_ID_SPIDER_FIELD = 9;
        public const int LOCATION_ID_DUNGEON_ENTRANCE = 10;

        public const int UNSELLABLE_ITEM_PRICE = -1;

        static World()
        {
            PopulateItems();
            PopulateMonsters();
            PopulateQuests();
            PopulateLocations();
        }

        private static void PopulateItems()
        {//create Items
            Items.Add(new Weapons(ITEM_ID_RUSTY_SWORD, "Rusty sword", "Rusty swords", 0, 5, 0));
            Items.Add(new Items(ITEM_ID_RAT_TAIL, "Rat tail", "Rat tails", 1));
            Items.Add(new Items(ITEM_ID_PIECE_OF_FUR, "Piece of fur", "Pieces of fur", 1));
            Items.Add(new Items(ITEM_ID_SNAKE_FANG, "Snake fang", "Snake fangs", 0.5f));
            Items.Add(new Items(ITEM_ID_SNAKESKIN, "Snakeskin", "Snakeskins", 0.5f));
            Items.Add(new Weapons(ITEM_ID_CLUB, "Club", "Clubs", 3, 10, 2.5f));
            Items.Add(new Potion(ITEM_ID_HEALING_POTION, "Healing potion", "Healing potions", 5, 50));
            Items.Add(new Items(ITEM_ID_SPIDER_FANG, "Spider fang", "Spider fangs", 2));
            Items.Add(new Items(ITEM_ID_SPIDER_SILK, "Spider silk", "Spider silks", 3));
            Items.Add(new Items(ITEM_ID_ADVENTURER_PASS, "Adventurer pass", "Adventurer passes", UNSELLABLE_ITEM_PRICE));
            Items.Add(new Weapons(ITEM_ID_SPIKED_CLUB, "Spiked club", "Spiked clubs", 5, 12, 25));
            Items.Add(new Weapons(ITEM_ID_GOLDEN_COLT, "Golden colt", "Madness", 15, 300, 30000));
            Items.Add(new Items(ITEM_ID_LEATHER, "Leather", "Leathers", 7));
            Items.Add(new Items(ITEM_ID_ROTTEN_FLESH, "Rotten flesh", "Pile of rotten flesh", 5));
            Items.Add(new Items(ITEM_ID_BONE, "Bone", "Bones", 5));
            Items.Add(new Weapons(ITEM_ID_LANCE, "Lance", "Lances", 10, 15, 30));
        }

        private static void PopulateMonsters()
        {//create Enemies
            Enemies rat = new Enemies(MONSTER_ID_RAT, "Rat", 5, 3, 8, 3, 3);
            rat.loot.Add(new Loot(ItemByID(ITEM_ID_RAT_TAIL), 75, false));
            rat.loot.Add(new Loot(ItemByID(ITEM_ID_PIECE_OF_FUR), 75, true));
            rat.loot.Add(new Loot(ItemByID(ITEM_ID_SPIKED_CLUB), 15, false));

            Enemies snake = new Enemies(MONSTER_ID_SNAKE, "Snake", 5, 3, 8, 3, 3);
            snake.loot.Add(new Loot(ItemByID(ITEM_ID_SNAKE_FANG), 75, false));
            snake.loot.Add(new Loot(ItemByID(ITEM_ID_SNAKESKIN), 75, true));
            snake.loot.Add(new Loot(ItemByID(ITEM_ID_SPIKED_CLUB), 20, false));

            Enemies giantSpider = new Enemies(MONSTER_ID_GIANT_SPIDER, "Giant spider", 20, 5, 25, 10, 10);
            giantSpider.loot.Add(new Loot(ItemByID(ITEM_ID_SPIDER_FANG), 75, true));
            giantSpider.loot.Add(new Loot(ItemByID(ITEM_ID_SPIDER_SILK), 25, false));
            giantSpider.loot.Add(new Loot(ItemByID(ITEM_ID_GOLDEN_COLT), 5, false));

            Enemies troll = new Enemies(MONSTER_ID_TROLL, "Troll", 7, 4, 15, 5, 5);
            troll.loot.Add(new Loot(ItemByID(ITEM_ID_CLUB), 80, false));
            troll.loot.Add(new Loot(ItemByID(ITEM_ID_LEATHER), 90, true));
            troll.loot.Add(new Loot(ItemByID(ITEM_ID_SPIKED_CLUB), 40, false));

            Enemies zombie = new Enemies(MONSTER_ID_ZOMBIE, "Zombie", 15, 12, 12, 7, 15);
            zombie.loot.Add(new Loot(ItemByID(ITEM_ID_ROTTEN_FLESH), 75, true));
            zombie.loot.Add(new Loot(ItemByID(ITEM_ID_BONE), 50, false));

            Enemies skeleton = new Enemies(MONSTER_ID_SKELETON, "Skeleton", 8, 8, 17, 7, 0);
            skeleton.loot.Add(new Loot(ItemByID(ITEM_ID_BONE), 75, true));
            skeleton.loot.Add(new Loot(ItemByID(ITEM_ID_LANCE), 50, false));

            Monsters.Add(rat);
            Monsters.Add(snake);
            Monsters.Add(troll);
            Monsters.Add(giantSpider);
            Monsters.Add(zombie);
            Monsters.Add(skeleton);
        }

        private static void PopulateQuests()
        {//create Quests
            Quest clearAlchemistGarden =
                new Quest(
                    QUEST_ID_CLEAR_ALCHEMIST_GARDEN,
                    "Clear the alchemist's garden",
                    "Kill rats in the alchemist's garden and bring back 3 rat tails. You will receive a healing potion and 10 gold pieces.", 20, 10);

            clearAlchemistGarden.questReward.Add(new QuestReward(ItemByID(ITEM_ID_RAT_TAIL), 3));

            clearAlchemistGarden.rewardItem = ItemByID(ITEM_ID_HEALING_POTION);

            Quest clearFarmersField =
                new Quest(
                    QUEST_ID_CLEAR_FARMERS_FIELD,
                    "Clear the farmer's field",
                    "Kill snakes in the farmer's field and bring back 3 snake fangs. You will receive an adventurer's pass and 20 gold pieces.", 20, 20);

            clearFarmersField.questReward.Add(new QuestReward(ItemByID(ITEM_ID_SNAKE_FANG), 3));

            clearFarmersField.rewardItem = ItemByID(ITEM_ID_ADVENTURER_PASS);

            Quests.Add(clearAlchemistGarden);
            Quests.Add(clearFarmersField);
        }

        private static void PopulateLocations()
        {
            // Create each location
            Location home = new Location(LOCATION_ID_HOME, "Home", "Your house. You really need to clean up the place.", null, null, null);

            Location townSquare = new Location(LOCATION_ID_TOWN_SQUARE, "Town square", "You see a vendor in front of a fountain.", null, null, null);

            Vendor bobTheHillbilly = new Vendor("Bob the Hillbilly");
            bobTheHillbilly.AddItemToInventory(ItemByID(ITEM_ID_HEALING_POTION), 5);
            bobTheHillbilly.AddItemToInventory(ItemByID(ITEM_ID_GOLDEN_COLT), 1);
            townSquare.vendorAvailableHere = bobTheHillbilly;

            Location alchemistHut = new Location(LOCATION_ID_ALCHEMIST_HUT, "Alchemist's hut", "There are many strange plants on the shelves.", null, QuestByID(QUEST_ID_CLEAR_ALCHEMIST_GARDEN), null);
            //alchemistHut.questAvailableHere = QuestByID(QUEST_ID_CLEAR_ALCHEMIST_GARDEN);

            Location alchemistsGarden = new Location(LOCATION_ID_ALCHEMISTS_GARDEN, "Alchemist's garden", "Many plants are growing here.", null, null, MonsterByID(MONSTER_ID_RAT));
            //alchemistsGarden.enemiesAvailableHere = MonsterByID(MONSTER_ID_RAT);

            Location farmhouse = new Location(LOCATION_ID_FARMHOUSE, "Farmhouse", "There is a small farmhouse, with a farmer in front.", null, QuestByID(QUEST_ID_CLEAR_FARMERS_FIELD), null);

            Location farmersField = new Location(LOCATION_ID_FARM_FIELD, "Farmer's field", "You see rows of vegetables growing here.", null, null, MonsterByID(MONSTER_ID_SNAKE));

            Location guardPost = new Location(LOCATION_ID_GUARD_POST, "Guard post", "There is a large, tough-looking guard here.", ItemByID(ITEM_ID_ADVENTURER_PASS), null, null);

            Location bridge = new Location(LOCATION_ID_BRIDGE, "Bridge", "A stone bridge crosses a wide river.", null, null, MonsterByID(MONSTER_ID_TROLL));

            Location spiderField = new Location(LOCATION_ID_SPIDER_FIELD, "Forest", "You see spider webs covering covering the trees in this forest.", null, null, MonsterByID(MONSTER_ID_GIANT_SPIDER));

            Location dungeonEntrance = new Location(LOCATION_ID_DUNGEON_ENTRANCE, "Cave", "You see an entrance to a cave behinde the dead spider body.", null, null, null);
            //skeleton overwritten by zombie
            dungeonEntrance.enemiesAvailableHere = MonsterByID(MONSTER_ID_SKELETON);
            dungeonEntrance.enemiesAvailableHere = MonsterByID(MONSTER_ID_ZOMBIE);


            // Link the locations together
            home.locationToNorth = townSquare;

            townSquare.locationToNorth = alchemistHut;
            townSquare.locationToSouth = home;
            townSquare.locationToEast = guardPost;
            townSquare.locationToWest = farmhouse;

            farmhouse.locationToEast = townSquare;
            farmhouse.locationToWest = farmersField;

            farmersField.locationToEast = farmhouse;

            alchemistHut.locationToSouth = townSquare;
            alchemistHut.locationToNorth = alchemistsGarden;

            alchemistsGarden.locationToSouth = alchemistHut;

            guardPost.locationToEast = bridge;
            guardPost.locationToWest = townSquare;

            bridge.locationToWest = guardPost;
            bridge.locationToEast = spiderField;

            spiderField.locationToWest = bridge;
            spiderField.locationToEast = dungeonEntrance;

            dungeonEntrance.locationToWest = spiderField;

            // Add the locations to the static list
            Locations.Add(home);
            Locations.Add(townSquare);
            Locations.Add(guardPost);
            Locations.Add(alchemistHut);
            Locations.Add(alchemistsGarden);
            Locations.Add(farmhouse);
            Locations.Add(farmersField);
            Locations.Add(bridge);
            Locations.Add(spiderField);
            Locations.Add(dungeonEntrance);
        }
        //check id number and return which object corresponds to it
        public static Items ItemByID(int id)
        {
            foreach (Items item in Items)
            {
                if (item.id == id)
                {
                    return item;
                }
            }

            return null;
        }

        public static Enemies MonsterByID(int id)
        {
            foreach (Enemies monster in Monsters)
            {
                if (monster.id == id)
                {
                    return monster;
                }
            }

            return null;
        }

        public static Quest QuestByID(int id)
        {
            foreach (Quest quest in Quests)
            {
                if (quest.id == id)
                {
                    return quest;
                }
            }

            return null;
        }

        public static Location LocationByID(int id)
        {
            foreach (Location location in Locations)
            {
                if (location.id == id)
                {
                    return location;
                }
            }

            return null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine
{
    public class Vendor : INotifyPropertyChanged
    {
        public string Name;
        public BindingList<Inventory> Inventory;

        public Vendor(string name)
        {
            Name = name;
            Inventory = new BindingList<Inventory>();
        }

        public void AddItemToInventory(Items itemToAdd, int quantity = 1)
        {
            Inventory item = Inventory.SingleOrDefault(ii => ii.details.id == itemToAdd.id);

            if (item == null)
            {
                // They didn't have the item, so add it to their inventory
                Inventory.Add(new Inventory(itemToAdd, quantity));
            }
            else
            {
                // They have the item in their inventory, so increase the quantity
                item.quantity += quantity;
            }

            OnPropertyChanged("Inventory");
        }

        public void RemoveItemFromInventory(Items itemToRemove, int quantity = 1)
        {
            Inventory item = Inventory.SingleOrDefault(ii => ii.details.id == itemToRemove.id);

            if (item == null)
            {
                // The item is not in the player's inventory, so ignore it.
                // We might want to raise an error for this situation
            }
            else
            {
                // They have the item in their inventory, so decrease the quantity
                item.quantity -= quantity;

                // Don't allow negative quantities.
                // We might want to raise an error for this situation
                if (item.quantity < 0)
                {
                    item.quantity = 0;
                }

                // If the quantity is zero, remove the item from the list
                if (item.quantity == 0)
                {
                    Inventory.Remove(item);
                }

                // Notify the UI that the inventory has changed
                OnPropertyChanged("Inventory");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}

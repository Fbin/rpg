﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine
{
    public class Items
    {
        private int _id;
        private string _name;
        
        public string nameplural;
        public float price;

        public Items(int id, string name, string nameplural, float price)
        {
            this._id = id;
            this._name = name;
            this.nameplural = nameplural;
            this.price = price;
        }

        public int id
        {
            get { return _id; }
        }
        public string name
        {
            get { return _name; }
        }
    }
}

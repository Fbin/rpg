﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Engine
{
    public class Player : LivingCreatures
    {
        private float _gold;
        private int _exp;
        private int _lvl;
        private Location _currentLocation;
        private Enemies enemie;

        public event EventHandler<MessageEventArgs> OnMessage;

        public float gold
        {
            get { return _gold; }
            set
            {
                _gold = value;
                OnPropertyChanged("gold");
            }
        }
        public int exp
        {
            get { return _exp; }
            set
            {
                _exp = value;
                OnPropertyChanged("exp");
            }
        }
        public int lvl
        {
            get { return _lvl; }
            set
            {
                _lvl = value;
                OnPropertyChanged("lvl");
            }
        }

        public BindingList<Inventory> inventory;
        public BindingList<PlayerQuest> playerQuest;
        public Location currentLocation
        {
            get { return _currentLocation; }
            set
            {
                _currentLocation = value;
                OnPropertyChanged("CurrentLocation");
            }
        }
        public Weapons currentWeapon;
        public List<Weapons> weapons
        {
            get { return inventory.Where(x => x.details is Weapons).Select(x => x.details as Weapons).ToList(); }
        }
        public List<Potion> potions
        {
            get { return inventory.Where(x => x.details is Potion).Select(x => x.details as Potion).ToList(); }
        }

        public Player(int hp, int maxHp, float gold, int exp, int lvl) : base(hp, maxHp)
        {
            this.gold = gold;
            this.exp = exp;
            this.lvl = lvl;
            inventory = new BindingList<Inventory>();
            playerQuest = new BindingList<PlayerQuest>();
        }

        public void lvlUp()
        {
            lvl++;
            exp = exp - 100;
            maxHp = maxHp + 10;
        }

        public static Player createDefaultPlayer()
        {
            Player player = new Player(10, 10, 20, 0, 1);
            player.inventory.Add(new Inventory(World.ItemByID(World.ITEM_ID_RUSTY_SWORD), 1));
            player.currentLocation = World.LocationByID(World.LOCATION_ID_HOME);

            return player;
        }

        public static Player CreatePlayerFromXmlString(string xmlPlayerData)
        {
            try
            {
                XmlDocument playerData = new XmlDocument();

                playerData.LoadXml(xmlPlayerData);

                int currentHitPoints = Convert.ToInt32(playerData.SelectSingleNode("/Player/Stats/CurrentHitPoints").InnerText);
                int maximumHitPoints = Convert.ToInt32(playerData.SelectSingleNode("/Player/Stats/MaximumHitPoints").InnerText);
                int gold = Convert.ToInt32(playerData.SelectSingleNode("/Player/Stats/Gold").InnerText);
                int experiencePoints = Convert.ToInt32(playerData.SelectSingleNode("/Player/Stats/ExperiencePoints").InnerText);
                int lvl = Convert.ToInt32(playerData.SelectSingleNode("/Player/Stats/Level").InnerText);

                Player player = new Player(currentHitPoints, maximumHitPoints, gold, experiencePoints, lvl);

                int currentLocationID = Convert.ToInt32(playerData.SelectSingleNode("/Player/Stats/CurrentLocation").InnerText);
                player.currentLocation = World.LocationByID(currentLocationID);

                if (playerData.SelectSingleNode("/Player/Stats/CurrentWeapon") != null)
                {
                    int currentWeapon = Convert.ToInt32(playerData.SelectSingleNode("/Player/Stats/CurrentWeapon").InnerText);
                    player.currentWeapon = (Weapons)World.ItemByID(currentWeapon);
                }

                foreach (XmlNode node in playerData.SelectNodes("/Player/InventoryItems/InventoryItem"))
                {
                    int id = Convert.ToInt32(node.Attributes["ID"].Value);
                    int quantity = Convert.ToInt32(node.Attributes["Quantity"].Value);

                    for (int i = 0; i < quantity; i++)
                    {
                        player.AddItemToInventory(World.ItemByID(id));
                    }
                }

                foreach (XmlNode node in playerData.SelectNodes("/Player/PlayerQuests/PlayerQuest"))
                {
                    int id = Convert.ToInt32(node.Attributes["ID"].Value);
                    bool isCompleted = Convert.ToBoolean(node.Attributes["IsCompleted"].Value);

                    PlayerQuest playerQuest = new PlayerQuest(World.QuestByID(id));
                    playerQuest.isCompleted = isCompleted;

                    player.playerQuest.Add(playerQuest);
                }

                return player;
            }
            catch
            {
                // If there was an error with the XML data, return a default player object
                return Player.createDefaultPlayer();
            }
        }

        public bool hasRequredItemToEnterThisLocation(Location location)
        {
            if(location.requiredToEnter == null)
            {
                return true;
            }

            // See if the player has the required item in their inventory
            /*foreach (Inventory ii in inventory)
            {
                if (ii.details.id == location.requiredToEnter.id)
                {
                    // We found the required item, so return "true"
                    return true;
                }
            }

            //required item not available
            return false;*/
            return inventory.Any(ii => ii.details.id == location.requiredToEnter.id);
        }

        /*public bool hasRequiredLvlToEnterThisLocation(Location location)
        {
            if(location.requiredLvlToEnter == null)
            {
                return true;
            }

            if (lvl = location.requiredLvlToEnter)
            {
                return true;
            }

            return false;
        }*/

        public bool HasThisQuest(Quest quest)
        {
            return playerQuest.Any(playerQuest => playerQuest.details.id == quest.id);
        }

        public bool CompletedThisQuest(Quest quest)
        {
            foreach(PlayerQuest playerQuest in playerQuest)
            {
                if(playerQuest.details.id == quest.id)
                {
                    return playerQuest.isCompleted;
                }
            }

            return false;
        }

        public bool HasAllQuestCompletionItems(Quest quest)
        {
            // See if the player has all the items needed to complete the quest here
            foreach (QuestReward qci in quest.questReward)
            {
                /*bool foundItemInPlayersInventory = false;

                // Check each item in the player's inventory, to see if they have it, and enough of it
                foreach (Inventory ii in inventory)
                {
                    if (ii.details.id == qci.details.id) // The player has the item in their inventory
                    {
                        foundItemInPlayersInventory = true;

                        if (ii.quantity < qci.quantity) // The player does not have enough of this item to complete the quest
                        {
                            return false;
                        }
                    }
                }*/
                

                // The player does not have any of this quest completion item in their inventory
                if (!inventory.Any(ii => ii.details.id == qci.details.id && ii.quantity >= qci.quantity))
                {
                    return false;
                }
            }

            // If we got here, then the player must have all the required items, and enough of them, to complete the quest.
            return true;
        }

        public void RemoveQuestCompletionItems(Quest quest)
        {
            foreach (QuestReward qci in quest.questReward)
            {
                Inventory ii = inventory.SingleOrDefault(item => item.details.id == qci.details.id);
                    //foreach (Inventory ii in inventory)
                //{
                    if (ii != null)
                    {
                    // Subtract the quantity from the player's inventory that was needed to complete the quest
                    RemoveItemFromInventory(ii.details, qci.quantity);
                    }
                //}
            }
        }

        public void AddItemToInventory(Items itemToAdd, int quantity = 1)
        {
            Inventory item = inventory.SingleOrDefault(ii => ii.details.id == itemToAdd.id);
            if (item == null)
            {
                // They didn't have the item, so add it to their inventory, with a quantity of 1
                inventory.Add(new Inventory(itemToAdd, quantity));
            }
            else
            {
                // They have the item in their inventory, so increase the quantity by one
                item.quantity += quantity;
            }

            // We added the item, and are done, so get out of this function
            RaiseInventoryChangedEvent(itemToAdd);
        }

        public void RemoveItemFromInventory(Items itemToRemove, int quantity = 1)
        {
            Inventory item = inventory.SingleOrDefault(ii => ii.details.id == itemToRemove.id);

            if (item == null)
            {
                // The item is not in the player's inventory, so ignore it.
                // We might want to raise an error for this situation
            }
            else
            {
                // They have the item in their inventory, so decrease the quantity
                item.quantity -= quantity;

                // Don't allow negative quantities.
                // We might want to raise an error for this situation
                if (item.quantity < 0)
                {
                    item.quantity = 0;
                }

                // If the quantity is zero, remove the item from the list
                if (item.quantity == 0)
                {
                    inventory.Remove(item);
                }

                // Notify the UI that the inventory has changed
                RaiseInventoryChangedEvent(itemToRemove);
            }
        }

        private void RaiseInventoryChangedEvent(Items item)
        {
            if (item is Weapons)
            {
                OnPropertyChanged("Weapons");
            }

            if (item is Potion)
            {
                OnPropertyChanged("Potions");
            }
        }

        public void MarkQuestCompleted(Quest quest)
        {
            PlayerQuest pq = playerQuest.SingleOrDefault(playerQuest => playerQuest.details.id == quest.id);          
            if (pq != null)
            {
                // Mark it as completed
                pq.isCompleted = true;

                return; // We found the quest, and marked it complete, so get out of this function
            }            
        }

        private void RaiseMessage(string message, bool addExtraNewLine = false)
        {
            if (OnMessage != null)
            {
                OnMessage(this, new MessageEventArgs(message, addExtraNewLine));
            }
        }

        public void MoveTo(Location newLocation)
        {
            //Does the location have any required items
            if (!hasRequredItemToEnterThisLocation(newLocation))
            {
                RaiseMessage("You must have a " + newLocation.requiredToEnter.name + " to enter this location.");
                return;
            }

            // Update the player's current location
            currentLocation = newLocation;

            // Completely heal the player
            hp = maxHp;

            // Does the location have a quest?
            if (newLocation.questAvailableHere != null)
            {
                // See if the player already has the quest, and if they've completed it
                bool playerAlreadyHasQuest = HasThisQuest(newLocation.questAvailableHere);
                bool playerAlreadyCompletedQuest = CompletedThisQuest(newLocation.questAvailableHere);

                // See if the player already has the quest
                if (playerAlreadyHasQuest)
                {
                    // If the player has not completed the quest yet
                    if (!playerAlreadyCompletedQuest)
                    {
                        // See if the player has all the items needed to complete the quest
                        bool playerHasAllItemsToCompleteQuest = HasAllQuestCompletionItems(newLocation.questAvailableHere);

                        // The player has all items required to complete the quest
                        if (playerHasAllItemsToCompleteQuest)
                        {
                            // Display message
                            RaiseMessage("");
                            RaiseMessage("You complete the '" + newLocation.questAvailableHere.name + "' quest.");

                            // Remove quest items from inventory
                            RemoveQuestCompletionItems(newLocation.questAvailableHere);

                            // Give quest rewards
                            RaiseMessage("You receive: ");
                            RaiseMessage(newLocation.questAvailableHere.expGain + " experience points");
                            RaiseMessage(newLocation.questAvailableHere.reward + " gold");
                            RaiseMessage(newLocation.questAvailableHere.rewardItem.name, true);

                            exp += newLocation.questAvailableHere.expGain;
                            gold += newLocation.questAvailableHere.reward;
                            if (exp >= 100)
                            {
                                lvlUp();
                            }

                            // Add the reward item to the player's inventory
                            AddItemToInventory(newLocation.questAvailableHere.rewardItem);

                            // Mark the quest as completed
                            MarkQuestCompleted(newLocation.questAvailableHere);
                        }
                    }
                }
                else
                {
                    // The player does not already have the quest

                    // Display the messages
                    RaiseMessage("You receive the " + newLocation.questAvailableHere.name + " quest.");
                    RaiseMessage(newLocation.questAvailableHere.description);
                    RaiseMessage("To complete it, return with:");
                    foreach (QuestReward qci in newLocation.questAvailableHere.questReward)
                    {
                        if (qci.quantity == 1)
                        {
                            RaiseMessage(qci.quantity + " " + qci.details.name);
                        }
                        else
                        {
                            RaiseMessage(qci.quantity + " " + qci.details.nameplural);
                        }
                    }
                    RaiseMessage("");

                    // Add the quest to the player's quest list
                    playerQuest.Add(new PlayerQuest(newLocation.questAvailableHere));
                }
            }

            // Does the location have a monster?
            if (newLocation.enemiesAvailableHere != null)
            {
                RaiseMessage("You see a " + newLocation.enemiesAvailableHere.name);

                // Make a new monster, using the values from the standard monster in the World.Monster list
                Enemies standardMonster = World.MonsterByID(newLocation.enemiesAvailableHere.id);

                enemie = new Enemies(standardMonster.id, standardMonster.name, standardMonster.dmg,
                    standardMonster.expGain, standardMonster.reward, standardMonster.hp, standardMonster.maxHp);

                foreach (Loot lootItem in standardMonster.loot)
                {
                    enemie.loot.Add(lootItem);
                }
            }
            else
            {
                enemie = null;
            }
        }

        public void UseWeapon(Weapons weapon)
        {
            // Determine the amount of damage to do to the monster
            int damageToMonster = RandomNumberGenerator.NumberBetween(weapon.minDmg, weapon.maxDmg);

            // Apply the damage to the monster's CurrentHitPoints
            enemie.hp -= damageToMonster;

            // Display message
            RaiseMessage("You hit the " + enemie.name + " for " + damageToMonster + " points.");

            // Check if the monster is dead
            if (enemie.hp <= 0)
            {
                // Monster is dead
                RaiseMessage("");
                RaiseMessage("You defeated the " + enemie.name);

                // Give player experience points for killing the monster
                exp += enemie.expGain;                
                RaiseMessage("You receive " + enemie.expGain + " experience points");
                if (exp >= 100)
                {
                    lvlUp();
                }

                // Give player gold for killing the monster 
                gold += enemie.reward;
                RaiseMessage("You receive " + enemie.reward + " gold");

                // Get random loot items from the monster
                List<Inventory> lootedItems = new List<Inventory>();

                // Add items to the lootedItems list, comparing a random number to the drop percentage
                foreach (Loot lootItem in enemie.loot)
                {
                    if (RandomNumberGenerator.NumberBetween(1, 100) <= lootItem.dropRate)
                    {
                        lootedItems.Add(new Inventory(lootItem.details, 1));
                    }
                }

                // If no items were randomly selected, then add the default loot item(s).
                if (lootedItems.Count == 0)
                {
                    foreach (Loot lootItem in enemie.loot)
                    {
                        if (lootItem.defaultItem)
                        {
                            lootedItems.Add(new Inventory(lootItem.details, 1));
                        }
                    }
                }

                // Add the looted items to the player's inventory
                foreach (Inventory inventoryItem in lootedItems)
                {
                    AddItemToInventory(inventoryItem.details);

                    if (inventoryItem.quantity == 1)
                    {
                        RaiseMessage("You loot " + inventoryItem.quantity + " " + inventoryItem.details.name);
                    }
                    else
                    {
                        RaiseMessage("You loot " + inventoryItem.quantity + " " + inventoryItem.details.nameplural);
                    }
                }

                // Add a blank line to the messages box, just for appearance.
                RaiseMessage("");

                // Move player to current location (to heal player and create a new monster to fight)
                MoveTo(currentLocation);
            }
            else
            {
                // Monster is still alive

                // Determine the amount of damage the monster does to the player
                int damageToPlayer = RandomNumberGenerator.NumberBetween(0, enemie.dmg);

                // Display message
                RaiseMessage("The " + enemie.name + " did " + damageToPlayer + " points of damage.");

                // Subtract damage from player
                hp -= damageToPlayer;

                if (hp <= 0)
                {
                    // Display message
                    RaiseMessage("The " + enemie.name + " killed you.");

                    // Move player to "Home"
                    MoveHome();
                }
            }
        }

        public void UsePotion(Potion potion)
        {
            // Add healing amount to the player's current hit points
            hp = (hp + potion.healthBack);

            // CurrentHitPoints cannot exceed player's MaximumHitPoints
            if (hp > maxHp)
            {
                hp = maxHp;
            }

            // Remove the potion from the player's inventory
            RemoveItemFromInventory(potion, 1);

            // Display message
            RaiseMessage("You drink a " + potion.name);

            // Monster gets their turn to attack

            // Determine the amount of damage the monster does to the player
            int damageToPlayer = RandomNumberGenerator.NumberBetween(0, enemie.dmg);

            // Display message
            RaiseMessage("The " + enemie.name + " did " + damageToPlayer + " points of damage.");

            // Subtract damage from player
            hp -= damageToPlayer;

            if (hp <= 0)
            {
                // Display message
                RaiseMessage("The " + enemie.name + " killed you.");

                // Move player to "Home"
                MoveHome();
            }
        }

        private void MoveHome()
        {
            MoveTo(World.LocationByID(World.LOCATION_ID_HOME));
        }

        public void MoveNorth()
        {
            if (currentLocation.locationToNorth != null)
            {
                MoveTo(currentLocation.locationToNorth);
            }
        }

        public void MoveEast()
        {
            if (currentLocation.locationToEast != null)
            {
                MoveTo(currentLocation.locationToEast);
            }
        }

        public void MoveSouth()
        {
            if (currentLocation.locationToSouth != null)
            {
                MoveTo(currentLocation.locationToSouth);
            }
        }

        public void MoveWest()
        {
            if (currentLocation.locationToWest != null)
            {
                MoveTo(currentLocation.locationToWest);
            }
        }

        public string ToXmlString()
        {
            XmlDocument playerData = new XmlDocument();

            // Create the top-level XML node
            XmlNode player = playerData.CreateElement("Player");
            playerData.AppendChild(player);

            // Create the "Stats" child node to hold the other player statistics nodes
            XmlNode stats = playerData.CreateElement("Stats");
            player.AppendChild(stats);

            // Create the child nodes for the "Stats" node
            XmlNode currentHitPoints = playerData.CreateElement("CurrentHitPoints");
            currentHitPoints.AppendChild(playerData.CreateTextNode(this.hp.ToString()));
            stats.AppendChild(currentHitPoints);

            XmlNode maximumHitPoints = playerData.CreateElement("MaximumHitPoints");
            maximumHitPoints.AppendChild(playerData.CreateTextNode(this.maxHp.ToString()));
            stats.AppendChild(maximumHitPoints);

            XmlNode gold = playerData.CreateElement("Gold");
            gold.AppendChild(playerData.CreateTextNode(this.gold.ToString()));
            stats.AppendChild(gold);

            XmlNode experiencePoints = playerData.CreateElement("ExperiencePoints");
            experiencePoints.AppendChild(playerData.CreateTextNode(this.exp.ToString()));
            stats.AppendChild(experiencePoints);

            XmlNode lvl = playerData.CreateElement("Level");
            lvl.AppendChild(playerData.CreateTextNode(this.lvl.ToString()));
            stats.AppendChild(lvl);

            XmlNode currentLocation = playerData.CreateElement("CurrentLocation");
            currentLocation.AppendChild(playerData.CreateTextNode(this.currentLocation.id.ToString()));
            stats.AppendChild(currentLocation);

            if (currentWeapon != null)
            {
                XmlNode currentWeapon = playerData.CreateElement("CurrentWeapon");
                currentWeapon.AppendChild(playerData.CreateTextNode(this.currentWeapon.id.ToString()));
                stats.AppendChild(currentWeapon);
            }

            // Create the "InventoryItems" child node to hold each InventoryItem node
            XmlNode inventoryItems = playerData.CreateElement("InventoryItems");
            player.AppendChild(inventoryItems);

            // Create an "InventoryItem" node for each item in the player's inventory
            foreach (Inventory item in this.inventory)
            {
                XmlNode inventoryItem = playerData.CreateElement("InventoryItem");

                XmlAttribute idAttribute = playerData.CreateAttribute("ID");
                idAttribute.Value = item.details.id.ToString();
                inventoryItem.Attributes.Append(idAttribute);

                XmlAttribute quantityAttribute = playerData.CreateAttribute("Quantity");
                quantityAttribute.Value = item.quantity.ToString();
                inventoryItem.Attributes.Append(quantityAttribute);

                inventoryItems.AppendChild(inventoryItem);
            }

            // Create the "PlayerQuests" child node to hold each PlayerQuest node
            XmlNode playerQuests = playerData.CreateElement("PlayerQuests");
            player.AppendChild(playerQuests);

            // Create a "PlayerQuest" node for each quest the player has acquired
            foreach (PlayerQuest quest in this.playerQuest)
            {
                XmlNode playerQuest = playerData.CreateElement("PlayerQuest");

                XmlAttribute idAttribute = playerData.CreateAttribute("ID");
                idAttribute.Value = quest.details.id.ToString();
                playerQuest.Attributes.Append(idAttribute);

                XmlAttribute isCompletedAttribute = playerData.CreateAttribute("IsCompleted");
                isCompletedAttribute.Value = quest.isCompleted.ToString();
                playerQuest.Attributes.Append(isCompletedAttribute);

                playerQuests.AppendChild(playerQuest);
            }

            return playerData.InnerXml; // The XML document, as a string, so we can save the data to disk
        }
    }
}

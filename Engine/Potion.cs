﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine
{
    public class Potion : Items
    {
        public int healthBack;

        public Potion(int id, string name, string nameplural, int amountToHeal, float price) : base(id, name, nameplural, price)
        {
            this.healthBack = amountToHeal;
        }
    }
}

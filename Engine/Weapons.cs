﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine
{
    public class Weapons : Items
    {
        public int minDmg;
        public int maxDmg;

        public Weapons(int id, string name, string nameplural, int minDmg, int maxDmg, float price) : base (id, name, nameplural, price)
        {
            this.minDmg = minDmg;
            this.maxDmg = maxDmg;
        }
    }
}

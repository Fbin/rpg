﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine
{
    public class QuestReward
    {
        public Items details;
        public int quantity;

        public QuestReward(Items details, int quantity)
        {
            this.details = details;
            this.quantity = quantity;
        }
    }
}

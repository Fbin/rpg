﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine
{
    public class Location
    {
        public int id;
        public string name;
        public string description;
        public Items requiredToEnter;
        public Player requiredLvlToEnter;
        public Quest questAvailableHere;
        public Enemies enemiesAvailableHere;
        public Location locationToNorth;
        public Location locationToEast;
        public Location locationToSouth;
        public Location locationToWest;
        public Vendor vendorAvailableHere;

        public Location(int id, string name, string description, Items requiredToEnter, /*Player requiredLvlToEnter,*/ Quest questAvailableHere, Enemies enemiesAvailableHere)
        {
            this.id = id;
            this.name = name;
            this.description = description;
            this.requiredToEnter = requiredToEnter;
            //this.requiredLvlToEnter = requiredLvlToEnter;
            this.enemiesAvailableHere = enemiesAvailableHere;
            this.questAvailableHere = questAvailableHere;
        }
    }
}

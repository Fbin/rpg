﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Engine;
using System.IO;

namespace RPG
{
    public partial class MenuScreen : Form
    {
        private Player player;
        private const string PLAYER_DATA_FILE_NAME = "PlayerData.xml";


        public MenuScreen(Player player)
        {
            InitializeComponent();
        }

        private void btnContinue_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnNewGame_Click(object sender, EventArgs e)
        {
            player = Player.createDefaultPlayer();
        }

        private void btnOptions_Click(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            File.WriteAllText(PLAYER_DATA_FILE_NAME, player.ToXmlString());
        }

        private void btnExit_Click(object sender, EventArgs e)
        {

        }
    }
}
